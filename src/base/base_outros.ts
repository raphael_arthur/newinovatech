//Insert Mesa redondas, editais e outros eventos em conjunto;
export let outros = [
    {
        'nome': "Abertura",
        'evento': "FAV",
        'dia': new Date("September 26, 2017 17:00:00"),
        'duracao': 1,
        'local': "Anf. 9",
        'mesa': false,
        'linkedin': "",
        'area': "Inovação",
        'resumo': "Na abertura do iNOVATECH da FAV contaremos com a presença da Profª Ana Carla Bittencourt (Vice-Diretora do CDT), Profª Maria Emília Walter (Decana de Pesquisa e Inovação), Profª Simone Peracmanis (Diretora da FAV), Profª Selma Regina Maggiotto (Coordenadora do curso de Agronomia/FAV), Profª Lígia Maria Cantarino da Costa (Coordenadora do curso de Medicina Veterinária/FAV), Profª Mireya Eugencia Valencia Perafán (Coordenadora do curso de Gestão do Agronegócio/UnB) e o Prof. Jean Pierre Medaets (Coordenador da Área de Ciências Sociais Aplicadas e Agronegócios/FAV).",
        'img': "assets/img/pessoas/Icone-people.png"
    },
    {
        'nome': "Palestra Propriedade Intelectual",
        'evento': "FAV",
        'dia': new Date("September 26, 2017 18:00:00"),
        'duracao': 1,
        'local': "Anf. 9",
        'mesa': false,
        'linkedin': "",
        'area': "Patentes",
        'resumo': "A palestra será ministrada pela Lívia Araújo e pela Luiza Xavier, colaboradoras do CDT onde será discutido sobre Propriedade Intelectual e Patentes",
        'img': "assets/img/pessoas/Icone-people.png"
    },
    {
        'nome': "Bate-papo com Startups: Experiências e Resultados",
        'evento': "FAV",
        'dia': new Date("September 27, 2017 13:00:00"),
        'duracao': 1,
        'local': "Anf. 10",
        'mesa': false,
        'linkedin': "",
        'area': "Empreendedorismo",
        'resumo': "Na mesa redonda, empresários das empresas Tecflora, Parceagro e Bolsa do Mercado irão contar de forma descontraída quais são os desafios do mercado e o que eles fazem para superar as adversidades e inovar com sucesso.",
        'img': "assets/img/pessoas/Icone-people.png"
    },
    {
        'nome': "DESAFIO iNOVATECH",
        'evento': "FAV",
        'dia': new Date("September 28, 2017 19:00:00"),
        'duracao': 1,
        'local': "Anf. 9",
        'mesa': false,
        'linkedin': "",
        'area': "Inovação",
        'resumo': "",
        'img': "assets/img/pessoas/Icone-people.png"
    },
    
];