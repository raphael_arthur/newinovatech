export let minicursos = [
    {
        'nome': "Design Thinking: Criatividade e Inovação na Prática",
        'evento': "FAV",
        'dia': new Date("September 26, 2017 18:00:00"),
        'duracao': 2,
        'local': "ANF. 10",
        'mesa': false,
        'descricao': "O curso de Design Thinking será ministrado pelo Eduardo Nomura que trabalho no Banco do Brasil e irá trabalhar o método prático-criativo de soluções para problemas que é caracterizado como o processo de DESIGN THINKING ",
        'img': "assets/img/pessoas/Icone-people.png"
    },
    {
        'nome': "Maker Space Rural",
        'evento': "FAV",
        'dia': new Date("September 27, 2017 8:30:00"),
        'duracao': 3.5,
        'local': "ANF. 9",
        'mesa': false,
        'descricao': "",
        'img': "assets/img/pessoas/Icone-people.png"
    },
    {
        'nome': "BM Canvas: Transformando Minha Ideia em Negócio",
        'evento': "FAV",
        'dia': new Date("September 28, 2017 12:00:00"),
        'duracao': 2,
        'local': "ANF. 10",
        'mesa': false,
        'descricao': "",
        'img': "assets/img/pessoas/Icone-people.png"
    },
    
];