export let base_perguntas = [
    {
        'enunciado': "Enunciado pergunta 1",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 1
    },
    {
        'enunciado': "Enunciado pergunta 2",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 2
    },
    {
        'enunciado': "Enunciado pergunta 3",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 3
    },
    {
        'enunciado': "Enunciado pergunta 4",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 4
    },
    {
        'enunciado': "Enunciado pergunta 5",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 1
    },
    {
        'enunciado': "Enunciado pergunta 6",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 2
    },
    {
        'enunciado': "Enunciado pergunta 7",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 3
    },
    {
        'enunciado': "Enunciado pergunta 8",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 4
    },
    {
        'enunciado': "Enunciado pergunta 9",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 1
    },
    {
        'enunciado': "Enunciado pergunta 10",
        'alternativas':
        [
            "letra a",
            "letra b",
            "letra c",
            "letra d"
        ],
        'resposta': 2
    }

]