export let cursos = [
    {
        "nome":"ADMINISTRAÇÃO"
    },
    {
        "nome":"ADMINISTRAÇÃO PÚBLICA"
    },
    {
        "nome":"AGRONOMIA"
    },
    {
        "nome":"ARQUITETURA E URBANISMO"
    },
    {
        "nome":"ARQUIVOLOGIA"
    },
    {
        "nome":"ARTES CÊNICAS"
    },
    {
        "nome":"ARTES VISUAIS"
    },
    {
        "nome":"BIBLIOTECONOMIA"
    },
    {
        "nome":"BIOTECNOLOGIA"
    },
    {
        "nome":"CIÊNCIA DA COMPUTAÇÃO"
    },
    {
        "nome":"CIÊNCIA POLÍTICA"
    },
    {
        "nome":"CIÊNCIAS AMBIENTAIS"
    },
    {
        "nome":"CIÊNCIAS BIOLÓGICAS"
    },
    {
        "nome":"CIÊNCIAS ECONÔMICAS"
    },
    {
        "nome":"CIÊNCIAS SOCIAIS"
    },
    {
        "nome":"COMPUTAÇÃO"
    },
    {
        "nome":"COMUNICAÇÃO SOCIAL"
    },
    {
        "nome":"DESIGN"
    },
     {
        "nome":"DIREITO"
    },
    {
        "nome":"EDUCAÇÃO ARTÍSTICA"
    },
    {
        "nome":"EDUCAÇÃO FÍSICA"
    },
    {
        "nome":"ENFERMAGEM"
    },
    {
        "nome":"ENGENHARIA AMBIENTAL"
    },
    {
        "nome":"ENGENHARIA CIVIL"
    },
    {
        "nome":"ENGENHARIA DE COMPUTAÇÃO"
    },
    {
        "nome":"ENGENHARIA DE PRODUÇÃO"
    },
    {
        "nome":"ENGENHARIA DE REDES"
    },
     {
        "nome":"ENGENHARIA ELÉTRICA"
    },
    {
        "nome":"ENGENHARIA FLORESTAL"
    },
    {
        "nome":"ENGENHARIA MECÂNICA"
    },
    {
        "nome":"ENGENHARIA MECATRÔNICA"
    },
    {
        "nome":"ENGENHARIA QUÍMICA"
    },
    {
        "nome":"ESTATÍSTICA"
    },
     {
        "nome":"FARMÁCIA"
    },
     {
        "nome":"FILOSOFIA"
    },
    {
        "nome":"FÍSICA"
    },
    {
        "nome":"GEOFÍSICA"
    },
    {
        "nome":"GEOGRAFIA"
    },
     {
        "nome":"GEOLOGIA"
    },
     {
        "nome":"GESTÃO DE AGRONEGÓCIOS"
    },
    {
        "nome":"GESTÃO DE POLÍTICAS PÚBLICAS"
    },
    {
        "nome":"HISTÓRIA"
    },
    {
        "nome":"JORNALISMO"
    },
    {
        "nome":"LETRAS"
    },
    {
        "nome":"LETRAS-TRADUÇÃO"
    },
    {
        "nome":"LETRAS-TRADUÇÃO ESPANHOL"
    },
    {
        "nome":"LÍNGUA DE SINAIS BRASILEIRA"
    },
    {
        "nome":"LÍNGUAS ESTRANGEIRAS APLICADAS - MSI"
    },
     {
        "nome":"MATEMÁTICA"
    },
    {
        "nome":"MEDICINA"
    },
    {
        "nome":"MEDICINA VETERINÁRIA"
    },
    {
        "nome":"MUSEOLOGIA"
    },
    {
        "nome":"MÚSICA"
    },
    {
        "nome":"NUTRIÇÃO"
    },
    {
        "nome":"ODONTOLOGIA"
    },
    {
        "nome":"PSICOLOGIA"
    },
    {
        "nome":"QUÍMICA"
    },
    {
        "nome":"QUÍMICA TECNOLÓGICA"
    },
     {
        "nome":"SAÚDE COLETIVA"
    },
    {
        "nome":"SERVIÇO SOCIAL"
    },
    {
        "nome":"TEATRO"
    },
    {
        "nome":"TEORIA, CRÍTICA E HISTÓRIA DA ARTE"
    },
    {
        "nome":"TURISMO"
    },
    {
        "nome":"OUTROS"
    },
];










