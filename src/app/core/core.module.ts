import { NgModule, Optional, SkipSelf } from '@angular/core';

import { AuthService } from './services/auth.service';


//modules
import { SignupPageModule } from '../../pages/signup/signup.module';
import { LoginPageModule } from '../../pages/login/login.module';
/**
 * This core module exports and provide service instance for the aplication level
 * we import it once in the AppModule when the app starts and never import anywhere else.
 * Here we call services and components needed aplication wide (with only and only one instance)
 *
 * @export
 * @class CoreModule
 */

@NgModule({
    //declarations: [ ],
    
    imports: [  
                SignupPageModule,
                LoginPageModule
            ],

    //exports: [ ],

    providers: [ AuthService ]
})
export class CoreModule {


    /**
     * Creates an instance of CoreModule.
     * @param {CoreModule} parentModule
     *
     * SOME EXPLANATION IS NEED (FROM ANGULAR DOCUMENTATION)
     *
     * The constructor tells Angular to inject the CoreModule into itself. That seems dangerously circular.
     * The injection would be circular if Angular looked for CoreModule in the current injector.
     * The @SkipSelf decorator means "look for CoreModule in an ancestor injector, above me in the injector hierarchy."
     * If the constructor executes as intended in the AppModule, there is no ancestor injector that could provide an instance of CoreModule.
     * The injector should give up.
     * By default the injector throws an error when it can't find a requested provider.
     * The @Optional decorator means not finding the service is OK.
     * The injector returns null, the parentModule parameter is null, and the constructor concludes uneventfully.
     *
     * @memberOf CoreModule
     */
    constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
        console.log("Core Module Instance Created");
        if (parentModule) {
            throw new Error(
            'CoreModule is already loaded. Import it in the AppModule only');
        }
    }

}
