import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export abstract class ServerService {

    // protected serverUrlBase:string = '';
    protected serverUrlBase:string = 'http://localhost/';

    constructor(protected http:Http){
        console.log("Server Service ");
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

    /**
     * Send a HTTP request to the server
     *
     * @protected
     * @param {string} data
     * @param {string} url
     * @returns {Observable<JSON>}
     *
     * @memberOf ServerService
     */
    protected sendPostRequest(data:string,url:string):Observable<any>{

		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
		let options = new RequestOptions({ headers: headers });

		//let body = 'email=' + 'email' + '&password=' + 'password';
        console.log(this.serverUrlBase + url)
        console.log(data);
		//return this.http.post(this.serverUrlBase + url, data, options).map(response => response.json()).catch(this.handleError);
        return this.http.post(this.serverUrlBase + url, data, options).map(response => response).catch(this.handleError);
	}

    // public toUrlEncoded(json:JSON):string{

    // }
}
