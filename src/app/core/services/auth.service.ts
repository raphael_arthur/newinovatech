import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs/Rx';

import { Http } from '@angular/http';

import { ServerService } from './server.service';


@Injectable()
export class AuthService extends ServerService{

    constructor(http:Http){
        super(http);
    }

    public login(matricula:string, password:string):Observable<any>{

        return Observable.create( (observer: Observer<JSON>) => {

            let serverRequest:string = 'matricula=' + matricula + '&password=' + password;
            this.sendPostRequest(serverRequest,'server_inovatech/request/sign-in.php').subscribe( (data:JSON) => {
                console.log(data);
                if(data['userBlocked'] == 'true')
                {
                    sessionStorage.setItem('userBlocked','true')
                } 
                else 
                {
                    sessionStorage.removeItem('userBlocked');
                }
                //console.log(data['userToken']);
                let dataArray = JSON.parse(data['_body']);
                console.log(dataArray);
                console.log(dataArray['userToken']);
                sessionStorage.setItem('userToken',dataArray['userToken']);
                sessionStorage.setItem('isSigned', dataArray['isSigned']);
                sessionStorage.setItem('REMOTE_ADDR', dataArray['REMOTE_ADDR']);
                sessionStorage.setItem('HTTP_USER_AGENT', dataArray['HTTP_USER_AGENT']);
                sessionStorage.setItem('userID', dataArray['userID']);
                sessionStorage.setItem('matricula', dataArray['matricula']);
                console.log(sessionStorage);
                this.checkLogin().subscribe( data => {
                    observer.next(data);
                    observer.complete();
                });
            })
        });
    }

    public checkLogin():Observable<JSON>{
        return Observable.create( (observer: Observer<JSON>) => {
            this.sendPostRequest('userToken=' + sessionStorage.getItem('userToken') +
            '&isSigned=' + sessionStorage.getItem('isSigned') + 
            '&HTTP_USER_AGENT=' + sessionStorage.getItem('HTTP_USER_AGENT') + 
            '&REMOTE_ADDR=' + sessionStorage.getItem('REMOTE_ADDR'),
            'server_iae/request/auth_session.php').subscribe( (data:JSON) => {
                console.log(data);
                
                observer.next(data);
                observer.complete();
            })
        });
    }

}

