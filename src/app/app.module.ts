import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
//import { CloudSettings, CloudModule } from '@ionic/cloud-angular';

//---------------------------PAGES------------------------------------
import { InicialPage } from '../pages/inicial/inicial';
import { MinicursoPage } from '../pages/minicurso/minicurso';
import { PalestrantePage } from '../pages/palestrante/palestrante';
import { StartupPage } from '../pages/startup/startup';
import { MenuPage } from '../pages/menu/menu';
import { CronogramaPage } from '../pages/cronograma/cronograma';
import { ModalPage } from '../pages/cronograma/modal-content';
import { MapaPage }  from '../pages/mapa/mapa';
import { QuestionarioPage } from '../pages/questionario/questionario';
import { DesafioPage } from '../pages/desafio/desafio';
import { ParceirosPage } from '../pages/parceiros/parceiros';
//import { LoginPage } from '../pages/login/login';
//import { SignupPage } from '../pages/signup/signup';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


//----------------------CORE MODULE-------------------------------
import { CoreModule } from './core/core.module';

//-----------------------SERVICES---------------------------------
import { UserService } from '../services/user.service';
import { LogService } from '../services/log.service';
@NgModule({
  declarations: [
    MyApp,
    InicialPage,
    PalestrantePage,
    MinicursoPage,
    StartupPage,
    MenuPage,
    CronogramaPage,
    ModalPage,
    MapaPage,
    QuestionarioPage,
    DesafioPage,
    ParceirosPage,
    /*LoginPage,
    SignupPage,*/
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CoreModule,
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    InicialPage,
    PalestrantePage,
    MinicursoPage,
    StartupPage,
    MenuPage,
    CronogramaPage,
    ModalPage,
    MapaPage,
    QuestionarioPage,
    DesafioPage,
    ParceirosPage,
    /*LoginPage,
    SignupPage*/
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserService,
    LogService,
  ]
})
export class AppModule {}
