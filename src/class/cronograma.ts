export class Cronograma {
  public nome:string;
  public dia: Date;
  public duracao: number;
  public local: string;
  public desc: string;
  public img: string;

  constructor(name:string, day:Date, duration:number, local:string, desc:string, img:string)
  {
    this.nome = name;
    this.dia = day;
    this.duracao = duration;
    this.local = local;
    this.desc = desc;
    this.img = img;
  }
}