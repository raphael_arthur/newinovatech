export class Pergunta
{
    public enunciado:String;
    public alternativas:Array<String>;
    public resposta:number;

    constructor(enunciado:String, alternativas:Array<String>, resposta:number)
    {
        this.enunciado = enunciado;
        this.alternativas = alternativas;
        this.resposta = resposta;
    }
}