import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';
@Injectable()
export class LogService extends ServerService {

    public log_found:any;

    constructor(http:Http){
        super(http);
    }

    getAll():Observable<any>
    {
        console.log("estou dentro get ionic");
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_inovatech/request/log_getAll.php").subscribe( (data:JSON) => {
                console.log(data);
                this.log_found = JSON.parse(data['_body']);
                console.log(this.log_found);
                observer.next(true);
            });
            
        });
        
    }
}
