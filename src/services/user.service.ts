import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';
import { AlertController } from 'ionic-angular';

@Injectable()
export class UserService extends ServerService {

    public userData:any;
    public users_found:any;
    constructor(http:Http, private alertCtrl:AlertController){
        super(http);
    }

    createUser(formData:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + formData['name'] + '&matricula=' + formData['matricula'] + '&cpf=' + formData['cpf'] + '&birthdate=' + formData['birthdate'] + '&gender=' + 
                formData['gender'] + '&email=' + formData['email'] + '&password=' + formData['password'] + '&curso=' + formData['curso'] + '&occupation=' + formData['occupation'] + 
                '&university=' + formData['university'] + '&semester=' + formData['semester'] + '&question_know=' + formData['question_know'];
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_inovatech/request/signup.php").subscribe( (data:any) => {
                console.log(data);
                let dataArray = (data['_body']);
                console.log(dataArray);
                let expr = "Success";
                if(dataArray.indexOf(expr) !== -1)
                {
                    this.sucessSignup();
                }
                else
                {
                    this.failSignup();
                }
            });
            
        });
        
    }

    private sucessSignup():void
    {
        
        let alert = this.alertCtrl.create({
            title: 'Aviso',
            subTitle: 'Usuário Cadastrado com sucesso!!',
            buttons: ['Ok']
        });
        alert.present();
    }

    private failSignup():void
    {
        let alert = this.alertCtrl.create({
            title: 'Erro',
            subTitle: 'Tente Novamente',
            buttons: ['Ok']
        });
        alert.present();
    }

    getUser(user_id:string):Observable<any>
    {
        console.log("estou dentro getuser ionic");
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'user_id=' + user_id;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_inovatech/request/user_get.php").subscribe( (data:JSON) => {
                console.log(data);
                this.userData = JSON.parse(data['_body']);
                console.log(this.userData);
                observer.next(true);
            });
            
        });
    }

    getAllUsers():Observable<any>
    {
        console.log("estou dentro getuser ionic");
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = '';
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_iae/request/user_getAll.php").subscribe( (data:JSON) => {
                console.log(data);
                this.users_found = JSON.parse(data['_body']);
                console.log(this.userData);
                observer.next(true);
            });
            
        });
    }

    editUser(userData:any):Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'firstName=' + userData[0].nome + '&lastName=' + userData[0].sobrenome + '&ramal=' + 
                userData[0].ramal + '&id=' + userData[0].idusuario;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_iae/request/user_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    updatePrivilegio(user:any):Observable<any>
    {
        console.log(user.idusuario);
        let temp:any;
        if(user.privilegio == "1")
        {
            temp = 0;
        }
        else
        {
            temp = 1;
        }    
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'idusuario=' + user.idusuario + '&privilegio=' + temp;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_iae/request/user_updatePrivilegio.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }
}
