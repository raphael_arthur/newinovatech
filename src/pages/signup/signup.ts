import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { UserService } from '../../services/user.service';

import { cursos } from '../../base/cursos_unb';
/**
 * Generated class for the SignupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  private signup:FormGroup;
  //private setor_observer:any;
  private setores:any;
  //private testes:any;
  private cursos_list: Array<any>;

  constructor( private formBuilder: FormBuilder, private userService:UserService, ){
    this.cursos_list = cursos;
      this.signup = this.formBuilder.group({
          name: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
          matricula: [null, Validators.compose([Validators.required, Validators.pattern('[0-9 ]*')])],
          cpf: [null, Validators.compose([Validators.required, Validators.pattern('[0-9 ]*')])],
          birthdate: [null],
          gender: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
          email:[null, Validators.required],
          password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
          occupation: [null],
          curso: [null],
          university: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
          semester: [null],
          question_know: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
          
      });
      
          console.log("estou signup")
          //console.log(sessionStorage.getItem('userID'));
  }

  public sendData(){
      console.log("send_Data singup")
      console.log(this.signup.value);
      this.userService.createUser(this.signup.value).subscribe( data => {
          //console.log("dentro do subscribe");
      });
      console.log(this.signup.value);
  }
}

