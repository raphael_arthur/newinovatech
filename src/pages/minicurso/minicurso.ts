import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { minicursos } from '../../base/base_minicursos';

@Component({
  selector: 'page-minicurso',
  templateUrl: 'minicurso.html'
})
export class MinicursoPage {

  public minicurso_list: Array<any>;
  public local: string;

  constructor(public navCtrl: NavController, public params: NavParams) 
  {
    this.minicurso_list = minicursos;
    this.local = params.get('place');
    console.log(this.local);
    console.log(this.minicurso_list);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MinicursoPage');
  }

  add0(i)
  {
    if(i<10)
    {
      i = "0" + i;
    }
    return i;
  }

}
