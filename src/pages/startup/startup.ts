import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { startups } from '../../base/base_startup';


@Component({
  selector: 'page-startup',
  templateUrl: 'startup.html'
})
export class StartupPage {

  public startup_list: Array<any>;
  public local: string;

  constructor(public navCtrl: NavController, public params: NavParams) 
  {
    this.startup_list = startups;
    this.local = params.get('place');
    console.log(this.local);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartupPage');
  }

  add0(i)
  {
    if(i<10)
    {
      i = "0" + i;
    }
    return i;
  }

}
