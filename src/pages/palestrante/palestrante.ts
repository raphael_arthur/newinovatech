import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { palestrantes } from '../../base/base_palestrantes';


@Component({
  selector: 'page-palestrante',
  templateUrl: 'palestrante.html'
})
export class PalestrantePage {

  public palestrantes_list: Array<any>;
  public local: string;
  
  constructor(public navCtrl: NavController, public params: NavParams) 
  {
    this.palestrantes_list = palestrantes;
    this.local = params.get('place');
    console.log(this.local);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PalestrantePage');
  }

  add0(i)
  {
    if(i<10)
    {
      i = "0" + i;
    }
    return i;
  }

}
