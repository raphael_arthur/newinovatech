import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform, NavParams } from 'ionic-angular';
 
declare var google;
 
@Component({
  selector: 'mapa-page',
  templateUrl: 'mapa.html'
})
export class MapaPage {
  public local;
  @ViewChild('map') mapElement: ElementRef;
  map: any;
 
  constructor(public navCtrl: NavController, public platform: Platform, public params:NavParams) {
    this.local = params.get('place');
    this.ionViewDidLoad();
  }
 
  ionViewDidLoad(){
    this.loadMap();
  }
 
  loadMap(){
    let latLng: any;
    switch(this.local)
    {
      case "FCE":
      {
        latLng = new google.maps.LatLng(-15.844324, -48.101396);
        break;
      }
      case "FUP":
      {
        latLng = new google.maps.LatLng(-15.600652, -47.658158);
        break;
      }
      case "FT":
      {
        latLng = new google.maps.LatLng(-15.763787, -47.872037);
        break;
      }
      case "FGA":
      {
        latLng = new google.maps.LatLng(-15.989644, -48.045561);
        break;
      }
      case "IE":
      {
        latLng = new google.maps.LatLng(-15.758543, -47.869103);
        break;
      }
    }
    
 
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    //console.log(this.mapElement);
    this.platform.ready().then(() => {
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: this.map.getCenter()
      });
  
    let content = "<h4>iNOVATECH FCE!</h4>";          
  
    this.addInfoWindow(marker, content);
  }); 
    
    
 
  }

  addInfoWindow(marker, content)
  {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
  
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
 
  }


}
