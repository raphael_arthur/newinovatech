import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { MenuPage } from '../menu/menu';

@Component({
  selector: 'page-inicial',
  templateUrl: 'inicial.html'
})
export class InicialPage {

  public place: string;

  constructor(public navCtrl: NavController, public params: NavParams, public alertCtrl: AlertController) 
  {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InicialPage');
  }

  openEvent(place)
  {
    this.navCtrl.push(MenuPage, { place: place } );
  }

  emBreve()
  {
    let alert = this.alertCtrl.create({
      title: 'Em Breve!',
      subTitle: 'Aguarde pois ainda estamos trabalhando no evento!!',
      buttons: ['OK']
    });
    alert.present();
  }
}
