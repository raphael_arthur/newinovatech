import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl } from '@angular/forms';

//Classe
//import { Pergunta } from '../../class/pergunta';
import { base_perguntas } from '../../base/base_perguntas';
@Component({
  selector: 'page-questionario',
  templateUrl: 'questionario.html'
})
export class QuestionarioPage {
  lista_perguntas:Array<any>;
  langs;
  langForm;
  //variáveis que guardam as respostas do formulário;
  resposta:Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) 
  {
    //this.image = 'http://upload.wikimedia.org/wikipedia/commons/4/4a/Logo_2013_Google.png';
    this.lista_perguntas = base_perguntas;
    for(let i = 0; i<10; i++)
    {
      this.resposta.push(0);
    }

  }

  doSubmit(event)
  {
    console.log('Submitting form', this.resposta)
  }

  validarRespostas()
  {
    let count:number = 0;
    let k:number = 0;
    for (k=0; k<10; k++)
    {
      if(this.resposta[k] == this.lista_perguntas[k].resposta)
      {
        count++;
      }
    }
    console.log("Acertos: ", count);
    if(count > 6)
    {
      console.log("Parabens Acertou!!");  
    }
    else
    {
      count = 0;
      for (k=0; k<10; k++)
      {
        this.resposta[k] = 0;
      }
      console.log("Tente Novamente!!");
    }
  }
}
