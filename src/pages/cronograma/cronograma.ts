import { Component } from '@angular/core';
import { NavController, Platform, NavParams, AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ModalPage } from './modal-content';




//Páginas
//import { PalestrantePage } from '../palestrante/palestrante';
//import { MinicursoPage } from '../minicurso/minicurso';
//import { StartupPage } from '../startup/startup';

//DATA
import { startups } from '../../base/base_startup';
import { minicursos } from '../../base/base_minicursos';
import { palestrantes } from '../../base/base_palestrantes';
import { outros } from '../../base/base_outros';

//class
import { Cronograma } from '../../class/cronograma';


@Component({
  selector: 'page-cronograma',
  templateUrl: 'cronograma.html'
})

export class CronogramaPage {

  public startup_list;
  public minicurso_list;
  public palestrante_list;
  public outro_list;

  public evento;

  public calendario_list:Array<Cronograma>;

  public dia1: Array<Cronograma> = [];
  public dia2: Array<Cronograma> = [];
  public dia3: Array<Cronograma> = [];

  public n_dia: number;

  notifyTime: any;
  notifications: any[] = [];
  days: any[];
  chosenHours: number;
  chosenMinutes: number;

  constructor(public navCtrl: NavController, public params: NavParams, public platform: Platform, public alerCtrl: AlertController,/* private LocalNotifications: LocalNotifications,*/ public modalCtrl: ModalController) 
  {
    this.startup_list = startups;
    this.minicurso_list = minicursos;
    this.palestrante_list = palestrantes;
    this.outro_list = outros;

    this.evento = params.get('place');

    if(this.evento == "FCE" || this.evento == "IE")
    {
      this.n_dia = 2;
    }
    else
    {
      this.n_dia = 3;
    }
    
    this.montarCalendario(this.evento);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CronogramaPage');
  }

  add0(i:any)
  {
    if(i<10)
    {
      i = "0" + i;
    }
    return i;
  }


  openModal(evento:Cronograma){
    console.log(evento);
    let modal = this.modalCtrl.create(ModalPage, {evento: evento});
    modal.present();
  }

  filtrarEventos(evento:string)
  {
    this.calendario_list = [];
    
    for (let startup of this.startup_list)
    {
      if(startup.evento == this.evento && startup.mesa == false)
      {
        let temp = new Cronograma(startup.nome, startup.dia, startup.duracao, startup.local, startup.resumo, startup.img);
        this.calendario_list.push(temp);
      }
    }

    for (let palestrante of this.palestrante_list)
    {
      if(palestrante.evento == this.evento && palestrante.mesa == false)
      {
        let temp = new Cronograma(palestrante.nome, palestrante.dia, palestrante.duracao, palestrante.local, palestrante.resumo, palestrante.img);
        this.calendario_list.push(temp);
      }
    }

    for (let minicurso of this.minicurso_list)
    {
      if(minicurso.evento == this.evento && minicurso.mesa == false)
      {
        let temp = new Cronograma(minicurso.nome, minicurso.dia, minicurso.duracao, minicurso.local, minicurso.descricao, minicurso.img);
        this.calendario_list.push(temp);
      }
    }

    for (let mesa of this.outro_list)
    {
      if(mesa.evento == this.evento && mesa.mesa == false)
      {
        let temp = new Cronograma(mesa.nome, mesa.dia, mesa.duracao, mesa.local, mesa.resumo, mesa.img);
        this.calendario_list.push(temp);
      }
    }

    this.calendario_list = this.calendario_list.sort((evento1:Cronograma, evento2:Cronograma): number =>{
      if (evento1.dia < evento2.dia) return -1;
      else if (evento1.dia > evento2.dia) return 1;
      else 
      {
        if(evento1.dia < evento2.dia) return -1;
        if(evento1.dia > evento2.dia) return 1;
        return 0;
      }
      
    });

    return this.calendario_list;
  }

  pushDia(i,j: number)
  {
    if(j == 1)
    {
      this.dia1.push(this.calendario_list[i]);
    }
    else if(j == 2)
    {
      this.dia2.push(this.calendario_list[i]);
    }
    else
    {
      this.dia3.push(this.calendario_list[i]);
    }
  }

  montarCalendario(evento:string)
  {

    //filtra os eventos por local
    this.calendario_list = this.filtrarEventos(evento);
    console.log(this.calendario_list);
    //separa os eventos em 3 dias
    let j: number = 1;
    console.log(this.calendario_list.length);

    if(this.n_dia == 2)
    {
      for(let i = 0; i < this.calendario_list.length; i++)
      {
        if(i == 0)
        {
          this.pushDia(i,j);
        }
        else if(this.calendario_list[i].dia.getDate() > this.calendario_list[i-1].dia.getDate())
        {
          j++;
          this.pushDia(i,j);
        }
        else
        {
          this.pushDia(i,j);
        }
      }
    }
    else
    {
      for(let i = 0; i < this.calendario_list.length; i++)
      {
        if(i == 0)
        {
          this.pushDia(i,j);
        }
        else if(this.calendario_list[i].dia.getDate() > this.calendario_list[i-1].dia.getDate())
        {
          j++;
          this.pushDia(i,j);
        }
        else
        {
          this.pushDia(i,j);
        }
      }
    }
    console.log(this.dia3);

    
  }

}
