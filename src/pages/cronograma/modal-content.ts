import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

//class
import { Cronograma } from '../../class/cronograma';


@Component({
  selector: 'page-modal',
  templateUrl: 'modal-content.html'
})

export class ModalPage {
  public evento:Cronograma;
  
   constructor(public params: NavParams, public viewCtrl: ViewController)
  {
    console.log(this.params);
    this.evento = this.params.get('evento');
    console.log(this.evento);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}


