import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
//paginas
import { MinicursoPage } from '../minicurso/minicurso';
import { PalestrantePage } from '../palestrante/palestrante';
import { StartupPage } from '../startup/startup';
import { CronogramaPage } from '../cronograma/cronograma';
import { MapaPage } from '../mapa/mapa';
import { ParceirosPage } from '../parceiros/parceiros'
import { DesafioPage } from '../desafio/desafio';

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html'
})

export class MenuPage {

  public local;

  constructor(public navCtrl: NavController, public params: NavParams, public alerCtrl: AlertController) 
  {
    this.local = params.get('place');
    console.log("construtor");
  }

  ionViewDidLoad() {
    if(this.local != "FAV")
    {
      this.doAlert2();
    }
    console.log(this.local);
  }

  navegarPagina(id:any)
  {
    switch(id)
    {
      case "Cronograma":
      {
        this.navCtrl.push(CronogramaPage, { place: this.local } );
        break;
      }
      case "Palestrantes":
      {
        this.navCtrl.push(PalestrantePage, { place: this.local } );
        break;
      }
      case "Startups":
      {
        this.navCtrl.push(StartupPage, { place: this.local } );
        break;
      }
      case "Minicursos":
      {
        this.navCtrl.push(MinicursoPage, { place: this.local } );
        break;
      }
      case "desafio":
      {
        //this.navCtrl.push(DesafioPage, { place: this.local } );
        this.doAlert();
        break;
      }
      case "foodTrucks":
      {
        //this.navCtrl.push(PalestrantePage, { place: this.local } );
        this.doAlert();
        break;
      }
      case "parceiros":
      {
        //this.navCtrl.push(ParceirosPage, { place: this.local } );
        this.doAlert();
        break;
      }
      case "mapa":
      {
        this.navCtrl.push(MapaPage, {place: this.local});
      }
    }
  }

  doAlert() {
    let alert = this.alerCtrl.create({
      title: 'Em Breve!',
      message: 'Em breve teremos mais informações!!!',
      buttons: ['Ok']
    });
    alert.present()
  }

  doAlert2() 
  {
    let alert = this.alerCtrl.create({
      title: 'Atenção!',
      message: 'Estamos em constante atualização para trazer mais detalhes sobre a programação!!!',
      buttons: ['Ok']
    });
    alert.present()
  }
}

