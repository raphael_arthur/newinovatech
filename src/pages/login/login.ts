import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../../app/core/services/auth.service';
import { AlertController } from 'ionic-angular';

import { MenuPage } from '../menu/menu';
import { SignupPage } from '../signup/signup'; 
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private login : FormGroup;

    constructor( private alertCtrl:AlertController, private formBuilder: FormBuilder, private auth: AuthService, public navCtrl: NavController, public navParams: NavParams  /*private router: Router*/){
        //construct the FormBuilder to login forms
        this.login = this.formBuilder.group({
            matricula: [null, Validators.required],
            password: [null, Validators.required],
        });
    }

    public sendData():void{
        this.auth.login(this.login.value.matricula, this.login.value.password).subscribe(data => {
            //let token = sessionStorage.getItem('userToken');
            let dataArray = (data['_body']);
            console.log(sessionStorage);
            
            if(sessionStorage.getItem('userBlocked') == 'true'){
                this.presentAlert();
                //this.router.navigate(['/home']);
            } else if(sessionStorage.getItem('userToken') != "" ){
                console.log("TENHO TOKEN");
                this.navCtrl.push(MenuPage);
            }
            console.log(data);
            if(dataArray == "TRETA")
            {
                this.wrongPassword();
            }
        });
    }

    private wrongPassword():void
    {
        let alert = this.alertCtrl.create({
            title: 'Erro',
            subTitle: 'Usuário ou senha incorreta.',
            buttons: ['Ok']
        });
        alert.present();
    }

    private presentAlert():void {
        let alert = this.alertCtrl.create({
            title: 'Conta Bloqueada',
            subTitle: 'Por questões de segurança sua conta foi bloqueada. Por favor, envie um email para comunicacao@cdt.unb.br.',
            buttons: ['Entendi']
        });
        alert.present();
    }

    private goToSignup():void
    {
      this.navCtrl.push(SignupPage);
    }
}